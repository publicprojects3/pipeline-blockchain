package io.lunes.security

object SecurityChecker {

  var frozen: FrozenAssetList = FrozenAssetList(List.empty)

  var frozenAssetName: List[String] = List.empty

  val bannedAddress: BanAddressList = BanAddressList(
    List(
      BanAddress("37bNK3TBATu55TSG86Foge9ErvRNtCz7ymq"),
      BanAddress("37SQeypsDvETTWCec7eXa8hXkA5q7Zyo7HS"),
      BanAddress("37e2cAC4r2u4Cm4PLjfFymXEwUeni5TrQ6R"),
      BanAddress("37ZYm1A289tc8U2KczLmpaJU9N3YXT6oJSc"),
      BanAddress("37RQv3kJhMck2ddYSQgas6p6xhLJwqakGWp"),
    )
  )

  val essentialAssetName: List[String] = List("LUNES")
  addFrozenAssetName(essentialAssetName.head)

  def checkAddress(input: String): Boolean = bannedAddress.banned(input)

  def addFrozenAssetName(input: String): Unit =
    if (!frozenAssetName.exists(_ == input))
      frozenAssetName = frozenAssetName ++ List(input)

  def checkFrozenAsset(account: String, assetId: String): Boolean =
    frozen.checksWith(account, assetId)

}
