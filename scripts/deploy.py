from requests import post, delete


def create_container(**params: dict) -> bool:
    """
        @params
        url: url of the instance where the container will be deployed
        name: Name of the container that will be deployed
        image: Image name used by container
        node_port: Communication port with a node with others
        api_port: Access port to node API

    """
    url = f"http://{params['url']}:2375/v1.41/containers/create?name={params['name']}"
    payload = {
        "Image": params['image'],
        "ExposedPorts": {
            f"{params['api_port']}/tcp": {},
            f"{params['node_port']}/tcp": {}
        },
        "PortBindings": {
            f"{params['api_port']}/tcp": [
                {
                    "HostPort": params['api_port']
                }
            ],
            f"{params['node_port']}/tcp": [
                {
                    "HostPort": params['node_port']
                }
            ]
        },
        "RestartPolicy": {
            "Name": "always"
        }
    }

    response = post(url, json=payload)

    return True if response.status_code == 201 else False, response.text


def start_container(url: str, container_name: str) -> bool:
    """
        @params
        url: URL of the instance where the container will be deployed
        container_name: Name of the container ** already created ** what will be started
    """
    url = f"http://{url}:2375/v1.41/containers/{container_name}/start"

    response = post(url)

    return True if response.status_code == 204 else False


def remove_container(url: str, container_name: str) -> bool:
    """
        @params
        url: URL of the instance where the container will be deployed
        container_name: Name of the container ** already created ** what will be removed
    """
    url = f"http://{url}:2375/v1.41/containers/{container_name}?force=true"

    response = delete(url)

    return True if response.status_code == 204 else False, response.content


if __name__ == '__main__':
    import sys
    required_params = [
        'url',
        'image',
        'name',
        'node_port',
        'api_port']
    data: dict = {
        key: value for key, value in zip(required_params, sys.argv[1:])
    }

    if remove_container(data['url'], data['name']):
        print(
            f"[info] Old container ** {data['name']} ** removed successfully\n")

    response = create_container(**data)
    if response[0]:
        print(f"[info] Container ** {data['name']} ** created successfully\n")

        if start_container(data['url'], data['name']):
            print(
                f"[info] Container ** {data['name']} ** started successfully\n")
        else:
            raise Exception("[Error] Container was not started")
    else:
        raise Exception(f"[Error] Container was not created\n{response[1]}")
